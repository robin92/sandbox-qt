#include <chrono>

#include <QCoreApplication>
#include <QDebug>
#include <QTimer>

#include "main.hpp"

Foo::Foo(QObject* p) : QObject(p) {}

void Foo::onCall() {
    qDebug() << "foo";
}

template <class... Args> auto connect(Args&&... args) {
    return QObject::connect(std::forward<Args>(args)...);
}

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);

    connect(nullptr, &QTimer::timeout, [] { qDebug() << "Hello, world!"; });

    auto timer = new QTimer{&app};
    timer->start(std::chrono::milliseconds(std::chrono::seconds(2)).count());

#ifdef BREAK
    connect(timer, &QTimer::timeout, nullptr, &Foo::onCall); // ERR: null dereference!
#else
    connect(timer, SIGNAL(timeout()), nullptr, SLOT(onCall()));
#endif

    connect(nullptr, &QTimer::timeout, nullptr, &Foo::onCall);
    connect(nullptr, SIGNAL(timeout()), nullptr, SLOT(onCall()));

    return app.exec();
}
