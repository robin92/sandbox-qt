#pragma once

#include <QObject>

class Foo : public QObject {
    Q_OBJECT;

public:
    Foo(QObject* = nullptr);

public slots:
    void onCall();
};
