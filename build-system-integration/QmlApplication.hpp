#pragma once
#include <QObject>
#include <QUrl>

class QGuiApplication;
class QQmlApplicationEngine;

class QmlApplication : public QObject {
    Q_OBJECT;

    QUrl _rootQmlUrl;
    QGuiApplication *_app = nullptr;
    QQmlApplicationEngine *_engine = nullptr;

public:
    QmlApplication(int&, char**, const QUrl&);

    int exec() const;

private:
    void onQmlLoaded(const QObject*, const QUrl&) const;

signals:
    void loadingFailed() const;
};
