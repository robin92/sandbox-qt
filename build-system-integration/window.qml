import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true

    Text {
        font {
            pixelSize: 32
            bold: true
        }
        objectName: "display"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: parent
        text: "Hello, world!"
    }
}
