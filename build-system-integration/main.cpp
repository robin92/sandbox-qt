#include "QmlApplication.hpp"

int main(int argc, char** argv) {
    QmlApplication app(argc, argv, QUrl{"qrc:/window.qml"});
    return app.exec();
}
