#include "QmlApplication.hpp"

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "Controller.hpp"

QmlApplication::QmlApplication(int& argc, char** argv, const QUrl& url)
    : _rootQmlUrl(url)
    , _app(new QGuiApplication(argc, argv))
    , _engine(new QQmlApplicationEngine(_app))
{
    connect(this, &QmlApplication::loadingFailed, _app, &QGuiApplication::quit, Qt::QueuedConnection);
    connect(_engine, &QQmlApplicationEngine::objectCreated, this, &QmlApplication::onQmlLoaded);
}

int QmlApplication::exec() const
{
    _engine->load(_rootQmlUrl);
    return _app->exec();
}

void QmlApplication::onQmlLoaded(const QObject* root, const QUrl&) const
{
    if (not root) {
        emit loadingFailed();
        return;
    }
    new Controller{root->findChild<QObject*>("display")};
}
