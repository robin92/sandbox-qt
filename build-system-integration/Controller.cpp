#include "Controller.hpp"

#include <cassert>
#include <chrono>

#include <QDebug>
#include <QVariant>

namespace {

constexpr std::chrono::milliseconds INTERVAL = std::chrono::seconds(1);

}  // namespace

Controller::Controller(QObject* parent) : QObject(parent) {
    assert(parent && "parent must not be nullptr");
    auto fn = [this, parent] { parent->setProperty("text", QVariant(++_counter)); };

    connect(&_timer, &QTimer::timeout, fn);
    fn();

    _timer.start(INTERVAL.count());
}
