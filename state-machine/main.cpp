#include <iostream>

#include <QCoreApplication>

#include "singleton.hpp"
#include "MachineRegistry.hpp"

int main(int argc, char** argv) {
    QCoreApplication app(argc, argv);

    const auto& registry = singleton<MachineRegistry>();
    std::cerr << "registered machines count: " << registry.size() << "\n";

    auto args = app.arguments();
    args.removeFirst();
    for (auto&& arg : args) {
        registry.get(arg.toStdString()).start();
    }

    return app.exec();
}
