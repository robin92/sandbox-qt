#include <iostream>

#include <QFinalState>
#include <QTimer>

#include "singleton.hpp"
#include "MachineRegistry.hpp"

namespace {

constexpr auto MACHINE = "parallel";

const auto say = [](std::string msg) {
    return [=]() { std::cout << msg << "\n"; };
};

const auto dbg = [](std::string msg) {
    return [=]() { std::cerr << "[" << MACHINE << "] " << msg << "\n"; };
};

class ParallelState : public QState {
public:
    ParallelState(QState* parent = nullptr) : QState(QState::ParallelStates, parent) {}
};

const auto connect = [](auto&&... args) { QObject::connect(std::forward<decltype(args)>(args)...); };

/*
 *           groupSt
 *           --> 0x0 --> 0x1 --> 0x2 --> 0x3 --> 0x4------
 * startSt --+                                           +--> finalSt
 *           ------> 1x0 -------> 1x1 -------> 1x2 -------
 */
void setup(QStateMachine* machine) {
    connect(machine, &QStateMachine::started, dbg("machine started"));
    connect(machine, &QStateMachine::finished, dbg("machine finished"));

    std::vector<std::vector<QState*>> chains(2);
    auto groupSt = new ParallelState(machine);
    chains[0].resize(5);
    chains[0][0] = new QState(groupSt);
    chains[0][1] = new QState(chains[0][0]);
    chains[0][2] = new QState(chains[0][0]);
    chains[0][3] = new QState(chains[0][0]);
    chains[0][4] = new QState(chains[0][0]);
    chains[0][0]->setInitialState(chains[0][1]);
    chains[1].resize(3);
    chains[1][0] = new QState(groupSt);
    chains[1][1] = new QState(chains[1][0]);
    chains[1][2] = new QState(chains[1][0]);
    chains[1][0]->setInitialState(chains[1][1]);
    auto startSt = new QState(machine);
    auto finalSt = new QFinalState(machine);

    connect(chains[0][0], &QState::entered, say("chain:0 state:0"));
    connect(chains[0][1], &QState::entered, say("chain:0 state:1"));
    connect(chains[0][2], &QState::entered, say("chain:0 state:2"));
    connect(chains[0][3], &QState::entered, say("chain:0 state:3"));
    connect(chains[0][4], &QState::entered, say("chain:0 state:4"));
    connect(chains[1][0], &QState::entered, say("chain:1 state:0"));
    connect(chains[1][1], &QState::entered, say("chain:1 state:1"));
    connect(chains[1][2], &QState::entered, say("chain:1 state:2"));

    machine->setInitialState(startSt);
    startSt->addTransition(startSt, &QState::entered, groupSt);

    {
        auto timer = new QTimer(machine);
        connect(chains[0][1], &QState::entered, [=]() { timer->start(3000); });
        chains[0][1]->addTransition(timer, &QTimer::timeout, chains[0][2]);
        chains[0][2]->addTransition(timer, &QTimer::timeout, chains[0][3]);
        chains[0][3]->addTransition(timer, &QTimer::timeout, chains[0][4]);
        chains[0][4]->addTransition(timer, &QTimer::timeout, finalSt);
    }
    {
        auto timer = new QTimer(machine);
        connect(chains[1][1], &QState::entered, [=]() { timer->start(1000); });
        chains[1][1]->addTransition(timer, &QTimer::timeout, chains[1][2]);
        chains[1][2]->addTransition(timer, &QTimer::timeout, finalSt);
    }
}

std::unique_ptr<QStateMachine> machine() {
    auto machine = std::make_unique<QStateMachine>();
    setup(machine.get());
    return machine;
}

bool __reg_status = singleton<MachineRegistry>().add(MACHINE, machine());

}  // namespace
