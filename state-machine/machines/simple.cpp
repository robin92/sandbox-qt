#include <iostream>

#include <QTimer>
#include <QFinalState>

#include "singleton.hpp"
#include "MachineRegistry.hpp"

namespace {

constexpr auto MACHINE = "simple";

const auto say = [](std::string msg) {
    return [=]() { std::cout << msg << "\n"; };
};

const auto dbg = [](std::string msg) {
    return [=]() { std::cerr << "[" << MACHINE << "] " << msg << "\n"; };
};

std::unique_ptr<QStateMachine> machine() {
    auto machine = std::make_unique<QStateMachine>();
    QObject::connect(machine.get(), &QStateMachine::started, dbg("machine started"));
    QObject::connect(machine.get(), &QStateMachine::finished, dbg("machine finished"));

    const std::vector<QState*> state {
        new QState(machine.get()),
        new QState(machine.get()),
    };
    auto finalSt = new QFinalState(machine.get());

    auto timer = new QTimer(machine.get());
    timer->setSingleShot(true);

    QObject::connect(machine.get(), &QStateMachine::started, [=]() { timer->start(3000); });
    QObject::connect(state[1], &QState::entered, say("Hello, Bob!"));
    QObject::connect(finalSt, &QState::entered, say("Hi, Alice!"));

    machine->setInitialState(state[0]);
    state[0]->addTransition(timer, &QTimer::timeout, state[1]);
    state[1]->addTransition(state[1], &QState::entered, finalSt);

    return machine;
}

bool __reg_status = singleton<MachineRegistry>().add(MACHINE, machine());

}  // namespace
