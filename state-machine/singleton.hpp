#pragma once

template <class T> T& singleton() {
    static T obj;
    return obj;
}
