#include "MachineRegistry.hpp"

#include <cassert>
#include <iostream>

bool MachineRegistry::add(std::string name, std::unique_ptr<QStateMachine> sm) {
    assert(sm && "machine must not be null");
    std::cerr << "registering machine '" << sm.get() << "' as '" << name << "'\n";
    _machines.emplace(std::move(name), std::move(sm));
    return true;
}

QStateMachine& MachineRegistry::get(std::string name) const { return *_machines.at(name); }

bool MachineRegistry::empty() const { return _machines.empty(); }

size_t MachineRegistry::size() const { return _machines.size(); }
