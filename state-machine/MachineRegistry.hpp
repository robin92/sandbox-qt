#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include <QStateMachine>

class MachineRegistry {
    using Storage = std::unordered_map<std::string, std::unique_ptr<QStateMachine>>;

    Storage _machines;

public:
    using value_type = Storage::value_type;
    using const_iterator = Storage::const_iterator;

    bool add(std::string, std::nullptr_t) = delete;
    bool add(std::string, std::unique_ptr<QStateMachine>);

    const_iterator begin() const { return _machines.begin(); }
    const_iterator end() const { return _machines.end(); }
    const_iterator cbegin() const { return _machines.cbegin(); }
    const_iterator cend() const { return _machines.cend(); }

    QStateMachine& get(std::string) const;
    bool empty() const;
    size_t size() const;
};
